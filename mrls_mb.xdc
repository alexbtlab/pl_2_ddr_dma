set_property PACKAGE_PIN J1 [get_ports {DDR3_dq[0]}]
set_property PACKAGE_PIN H3 [get_ports {DDR3_dq[1]}]
set_property PACKAGE_PIN K1 [get_ports {DDR3_dq[2]}]
set_property PACKAGE_PIN G3 [get_ports {DDR3_dq[3]}]
set_property PACKAGE_PIN G4 [get_ports {DDR3_dq[4]}]
set_property PACKAGE_PIN G2 [get_ports {DDR3_dq[5]}]
set_property PACKAGE_PIN J5 [get_ports {DDR3_dq[6]}]
set_property PACKAGE_PIN H2 [get_ports {DDR3_dq[7]}]
set_property PACKAGE_PIN E2 [get_ports {DDR3_dq[8]}]
set_property PACKAGE_PIN F1 [get_ports {DDR3_dq[9]}]
set_property PACKAGE_PIN D2 [get_ports {DDR3_dq[10]}]
set_property PACKAGE_PIN E3 [get_ports {DDR3_dq[11]}]
set_property PACKAGE_PIN B1 [get_ports {DDR3_dq[12]}]
set_property PACKAGE_PIN B2 [get_ports {DDR3_dq[13]}]
set_property PACKAGE_PIN C2 [get_ports {DDR3_dq[14]}]
set_property PACKAGE_PIN A1 [get_ports {DDR3_dq[15]}]
set_property PACKAGE_PIN P6 [get_ports {DDR3_addr[13]}]
set_property PACKAGE_PIN L1 [get_ports {DDR3_addr[12]}]
set_property PACKAGE_PIN L3 [get_ports {DDR3_addr[11]}]
set_property PACKAGE_PIN L4 [get_ports {DDR3_addr[10]}]
set_property PACKAGE_PIN P1 [get_ports {DDR3_addr[9]}]
set_property PACKAGE_PIN M6 [get_ports {DDR3_addr[8]}]
set_property PACKAGE_PIN P2 [get_ports {DDR3_addr[7]}]
set_property PACKAGE_PIN N2 [get_ports {DDR3_addr[6]}]
set_property PACKAGE_PIN N4 [get_ports {DDR3_addr[5]}]
set_property PACKAGE_PIN M1 [get_ports {DDR3_addr[4]}]
set_property PACKAGE_PIN M3 [get_ports {DDR3_addr[3]}]
set_property PACKAGE_PIN N3 [get_ports {DDR3_addr[2]}]
set_property PACKAGE_PIN N5 [get_ports {DDR3_addr[1]}]
set_property PACKAGE_PIN M5 [get_ports {DDR3_addr[0]}]
set_property PACKAGE_PIN K4 [get_ports {DDR3_ba[2]}]
set_property PACKAGE_PIN M2 [get_ports {DDR3_ba[1]}]
set_property PACKAGE_PIN K3 [get_ports {DDR3_ba[0]}]
set_property PACKAGE_PIN L6 [get_ports DDR3_ras_n]
set_property PACKAGE_PIN J6 [get_ports DDR3_cas_n]
set_property PACKAGE_PIN K6 [get_ports DDR3_we_n]
set_property PACKAGE_PIN G1 [get_ports DDR3_reset_n]
set_property PACKAGE_PIN L5 [get_ports {DDR3_cke[0]}]
set_property PACKAGE_PIN R1 [get_ports {DDR3_odt[0]}]
set_property PACKAGE_PIN J4 [get_ports {DDR3_cs_n[0]}]
set_property PACKAGE_PIN H5 [get_ports {DDR3_dm[0]}]
set_property PACKAGE_PIN F3 [get_ports {DDR3_dm[1]}]
set_property PACKAGE_PIN K2 [get_ports {DDR3_dqs_p[0]}]
set_property PACKAGE_PIN J2 [get_ports {DDR3_dqs_n[0]}]
set_property PACKAGE_PIN E1 [get_ports {DDR3_dqs_p[1]}]
set_property PACKAGE_PIN D1 [get_ports {DDR3_dqs_n[1]}]
set_property PACKAGE_PIN P5 [get_ports {DDR3_ck_p[0]}]
set_property PACKAGE_PIN P4 [get_ports {DDR3_ck_n[0]}]
set_property INTERNAL_VREF 0.75 [get_iobanks 35]
set_property PACKAGE_PIN V4 [get_ports sys_clock]
set_property PACKAGE_PIN W1 [get_ports ETH_REF]
set_property PACKAGE_PIN A13 [get_ports {EN_AMP[0]}]
set_property PACKAGE_PIN AA1 [get_ports mdio_mdc]
set_property PACKAGE_PIN Y1 [get_ports mdio_mdio_io]
set_property PACKAGE_PIN R6 [get_ports {rmii_rxd[0]}]
set_property PACKAGE_PIN T6 [get_ports {rmii_rxd[1]}]
set_property PACKAGE_PIN W6 [get_ports {rmii_txd[0]}]
set_property PACKAGE_PIN W5 [get_ports {rmii_txd[1]}]
set_property PACKAGE_PIN V5 [get_ports rmii_crs_dv]
set_property PACKAGE_PIN AB3 [get_ports rmii_rx_er]
set_property PACKAGE_PIN AB1 [get_ports rmii_tx_en]
set_property PACKAGE_PIN U18 [get_ports SD_pin1_io]
set_property PACKAGE_PIN W20 [get_ports SD_pin2_io]
set_property PACKAGE_PIN V19 [get_ports SD_pin3_io]
set_property PACKAGE_PIN W19 [get_ports SD_pin4_io]
set_property PACKAGE_PIN T21 [get_ports SD_pin7_io]
set_property PACKAGE_PIN T20 [get_ports SD_pin8_io]
set_property PACKAGE_PIN T18 [get_ports SD_pin9_io]
set_property PACKAGE_PIN V22 [get_ports SD_pin10_io]
set_property PACKAGE_PIN U17 [get_ports UART_rxd]
set_property PACKAGE_PIN V17 [get_ports UART_txd]
set_property PACKAGE_PIN G17 [get_ports {ATTEN[0]}]
set_property PACKAGE_PIN H17 [get_ports {ATTEN[1]}]
set_property PACKAGE_PIN H15 [get_ports {ATTEN[2]}]
set_property PACKAGE_PIN J15 [get_ports {ATTEN[3]}]
set_property PACKAGE_PIN H18 [get_ports {ATTEN[4]}]
set_property PACKAGE_PIN G18 [get_ports {ATTEN[5]}]
set_property PACKAGE_PIN G15 [get_ports PLL_CEN]
set_property PACKAGE_PIN G16 [get_ports PLL_LD_SDO]
set_property PACKAGE_PIN J14 [get_ports PLL_TRIG]
set_property PACKAGE_PIN H14 [get_ports PLL_SEN]
set_property PACKAGE_PIN G13 [get_ports PLL_SCK]
set_property PACKAGE_PIN H13 [get_ports PLL_MOSI]
set_property PACKAGE_PIN AB18 [get_ports USER_LED]
set_property DIRECTION OUT [get_ports {ATTEN[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[5]}]
set_property DRIVE 12 [get_ports {ATTEN[5]}]
set_property SLEW SLOW [get_ports {ATTEN[5]}]
set_property DIRECTION OUT [get_ports {ATTEN[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[4]}]
set_property DRIVE 12 [get_ports {ATTEN[4]}]
set_property SLEW SLOW [get_ports {ATTEN[4]}]
set_property DIRECTION OUT [get_ports {ATTEN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[3]}]
set_property DRIVE 12 [get_ports {ATTEN[3]}]
set_property SLEW SLOW [get_ports {ATTEN[3]}]
set_property DIRECTION OUT [get_ports {ATTEN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[2]}]
set_property DRIVE 12 [get_ports {ATTEN[2]}]
set_property SLEW SLOW [get_ports {ATTEN[2]}]
set_property DIRECTION OUT [get_ports {ATTEN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[1]}]
set_property DRIVE 12 [get_ports {ATTEN[1]}]
set_property SLEW SLOW [get_ports {ATTEN[1]}]
set_property DIRECTION OUT [get_ports {ATTEN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ATTEN[0]}]
set_property DRIVE 12 [get_ports {ATTEN[0]}]
set_property SLEW SLOW [get_ports {ATTEN[0]}]
set_property DIRECTION OUT [get_ports {EN_AMP[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EN_AMP[0]}]
set_property DRIVE 12 [get_ports {EN_AMP[0]}]
set_property SLEW SLOW [get_ports {EN_AMP[0]}]
set_property DIRECTION OUT [get_ports PLL_MOSI]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_MOSI]
set_property DRIVE 12 [get_ports PLL_MOSI]
set_property SLEW SLOW [get_ports PLL_MOSI]
set_property DIRECTION OUT [get_ports PLL_SCK]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_SCK]
set_property DRIVE 12 [get_ports PLL_SCK]
set_property SLEW SLOW [get_ports PLL_SCK]
set_property DIRECTION OUT [get_ports PLL_SEN]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_SEN]
set_property DRIVE 12 [get_ports PLL_SEN]
set_property SLEW SLOW [get_ports PLL_SEN]
set_property DIRECTION OUT [get_ports PLL_TRIG]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_TRIG]
set_property DRIVE 12 [get_ports PLL_TRIG]
set_property SLEW SLOW [get_ports PLL_TRIG]
set_property DIRECTION OUT [get_ports USER_LED]
set_property IOSTANDARD LVCMOS33 [get_ports USER_LED]
set_property DRIVE 12 [get_ports USER_LED]
set_property SLEW SLOW [get_ports USER_LED]
set_property DIRECTION OUT [get_ports PLL_CEN]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_CEN]
set_property DRIVE 12 [get_ports PLL_CEN]
set_property SLEW SLOW [get_ports PLL_CEN]
set_property DIRECTION IN [get_ports PLL_LD_SDO]
set_property IOSTANDARD LVCMOS33 [get_ports PLL_LD_SDO]
set_property DIRECTION IN [get_ports sys_clock]
set_property IOSTANDARD LVCMOS33 [get_ports sys_clock]
set_property DIRECTION IN [get_ports {rmii_rxd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_rxd[1]}]
set_property DIRECTION IN [get_ports {rmii_rxd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_rxd[0]}]
set_property DIRECTION OUT [get_ports {rmii_txd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_txd[1]}]
set_property DRIVE 12 [get_ports {rmii_txd[1]}]
set_property SLEW SLOW [get_ports {rmii_txd[1]}]
set_property DIRECTION OUT [get_ports {rmii_txd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_txd[0]}]
set_property DRIVE 12 [get_ports {rmii_txd[0]}]
set_property SLEW SLOW [get_ports {rmii_txd[0]}]
set_property DIRECTION IN [get_ports rmii_crs_dv]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_crs_dv]
set_property DIRECTION IN [get_ports rmii_rx_er]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_rx_er]
set_property DIRECTION OUT [get_ports rmii_tx_en]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_tx_en]
set_property DRIVE 12 [get_ports rmii_tx_en]
set_property SLEW SLOW [get_ports rmii_tx_en]
set_property DIRECTION IN [get_ports UART_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports UART_rxd]
set_property DIRECTION OUT [get_ports UART_txd]
set_property IOSTANDARD LVCMOS33 [get_ports UART_txd]
set_property DRIVE 12 [get_ports UART_txd]
set_property SLEW SLOW [get_ports UART_txd]
set_property DIRECTION INOUT [get_ports SD_pin10_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin10_io]
set_property DRIVE 12 [get_ports SD_pin10_io]
set_property SLEW SLOW [get_ports SD_pin10_io]
set_property DIRECTION INOUT [get_ports SD_pin1_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin1_io]
set_property DRIVE 12 [get_ports SD_pin1_io]
set_property SLEW SLOW [get_ports SD_pin1_io]
set_property DIRECTION INOUT [get_ports SD_pin2_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin2_io]
set_property DRIVE 12 [get_ports SD_pin2_io]
set_property SLEW SLOW [get_ports SD_pin2_io]
set_property DIRECTION INOUT [get_ports SD_pin3_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin3_io]
set_property DRIVE 12 [get_ports SD_pin3_io]
set_property SLEW SLOW [get_ports SD_pin3_io]
set_property DIRECTION INOUT [get_ports SD_pin4_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin4_io]
set_property DRIVE 12 [get_ports SD_pin4_io]
set_property SLEW SLOW [get_ports SD_pin4_io]
set_property DIRECTION INOUT [get_ports SD_pin7_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin7_io]
set_property DRIVE 12 [get_ports SD_pin7_io]
set_property SLEW SLOW [get_ports SD_pin7_io]
set_property DIRECTION INOUT [get_ports SD_pin8_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin8_io]
set_property DRIVE 12 [get_ports SD_pin8_io]
set_property SLEW SLOW [get_ports SD_pin8_io]
set_property DIRECTION INOUT [get_ports SD_pin9_io]
set_property IOSTANDARD LVCMOS33 [get_ports SD_pin9_io]
set_property DRIVE 12 [get_ports SD_pin9_io]
set_property SLEW SLOW [get_ports SD_pin9_io]
set_property DIRECTION OUT [get_ports ETH_REF]
set_property IOSTANDARD LVCMOS33 [get_ports ETH_REF]
set_property DRIVE 12 [get_ports ETH_REF]
set_property SLEW SLOW [get_ports ETH_REF]
set_property DIRECTION OUT [get_ports mdio_mdc]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_mdc]
set_property DRIVE 12 [get_ports mdio_mdc]
set_property SLEW SLOW [get_ports mdio_mdc]
set_property DIRECTION INOUT [get_ports mdio_mdio_io]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_mdio_io]
set_property DRIVE 12 [get_ports mdio_mdio_io]
set_property SLEW SLOW [get_ports mdio_mdio_io]
set_property DIRECTION INOUT [get_ports {DDR3_dqs_n[1]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_n[1]}]
set_property SLEW FAST [get_ports {DDR3_dqs_n[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_n[1]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_n[1]}]
set_property DIRECTION INOUT [get_ports {DDR3_dqs_n[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_n[0]}]
set_property SLEW FAST [get_ports {DDR3_dqs_n[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_n[0]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_n[0]}]
set_property DIRECTION INOUT [get_ports {DDR3_dqs_p[1]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_p[1]}]
set_property SLEW FAST [get_ports {DDR3_dqs_p[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_p[1]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_p[1]}]
set_property DIRECTION INOUT [get_ports {DDR3_dqs_p[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_p[0]}]
set_property SLEW FAST [get_ports {DDR3_dqs_p[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_p[0]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_p[0]}]
set_property DIRECTION OUT [get_ports {DDR3_dm[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dm[1]}]
set_property SLEW FAST [get_ports {DDR3_dm[1]}]
set_property DIRECTION OUT [get_ports {DDR3_dm[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dm[0]}]
set_property SLEW FAST [get_ports {DDR3_dm[0]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[13]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[13]}]
set_property SLEW FAST [get_ports {DDR3_addr[13]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[12]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[12]}]
set_property SLEW FAST [get_ports {DDR3_addr[12]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[11]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[11]}]
set_property SLEW FAST [get_ports {DDR3_addr[11]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[10]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[10]}]
set_property SLEW FAST [get_ports {DDR3_addr[10]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[9]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[9]}]
set_property SLEW FAST [get_ports {DDR3_addr[9]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[8]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[8]}]
set_property SLEW FAST [get_ports {DDR3_addr[8]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[7]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[7]}]
set_property SLEW FAST [get_ports {DDR3_addr[7]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[6]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[6]}]
set_property SLEW FAST [get_ports {DDR3_addr[6]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[5]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[5]}]
set_property SLEW FAST [get_ports {DDR3_addr[5]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[4]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[4]}]
set_property SLEW FAST [get_ports {DDR3_addr[4]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[3]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[3]}]
set_property SLEW FAST [get_ports {DDR3_addr[3]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[2]}]
set_property SLEW FAST [get_ports {DDR3_addr[2]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[1]}]
set_property SLEW FAST [get_ports {DDR3_addr[1]}]
set_property DIRECTION OUT [get_ports {DDR3_addr[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[0]}]
set_property SLEW FAST [get_ports {DDR3_addr[0]}]
set_property DIRECTION OUT [get_ports {DDR3_cs_n[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_cs_n[0]}]
set_property SLEW FAST [get_ports {DDR3_cs_n[0]}]
set_property DIRECTION OUT [get_ports {DDR3_ck_n[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_ck_n[0]}]
set_property SLEW FAST [get_ports {DDR3_ck_n[0]}]
set_property DIRECTION OUT [get_ports {DDR3_ck_p[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_ck_p[0]}]
set_property SLEW FAST [get_ports {DDR3_ck_p[0]}]
set_property DIRECTION OUT [get_ports {DDR3_cke[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_cke[0]}]
set_property SLEW FAST [get_ports {DDR3_cke[0]}]
set_property DIRECTION OUT [get_ports {DDR3_ba[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[2]}]
set_property SLEW FAST [get_ports {DDR3_ba[2]}]
set_property DIRECTION OUT [get_ports {DDR3_ba[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[1]}]
set_property SLEW FAST [get_ports {DDR3_ba[1]}]
set_property DIRECTION OUT [get_ports {DDR3_ba[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[0]}]
set_property SLEW FAST [get_ports {DDR3_ba[0]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[15]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[15]}]
set_property SLEW FAST [get_ports {DDR3_dq[15]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[15]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[14]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[14]}]
set_property SLEW FAST [get_ports {DDR3_dq[14]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[14]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[13]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[13]}]
set_property SLEW FAST [get_ports {DDR3_dq[13]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[13]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[12]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[12]}]
set_property SLEW FAST [get_ports {DDR3_dq[12]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[12]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[11]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[11]}]
set_property SLEW FAST [get_ports {DDR3_dq[11]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[11]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[10]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[10]}]
set_property SLEW FAST [get_ports {DDR3_dq[10]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[10]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[9]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[9]}]
set_property SLEW FAST [get_ports {DDR3_dq[9]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[9]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[8]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[8]}]
set_property SLEW FAST [get_ports {DDR3_dq[8]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[8]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[7]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[7]}]
set_property SLEW FAST [get_ports {DDR3_dq[7]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[7]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[6]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[6]}]
set_property SLEW FAST [get_ports {DDR3_dq[6]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[6]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[5]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[5]}]
set_property SLEW FAST [get_ports {DDR3_dq[5]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[5]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[4]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[4]}]
set_property SLEW FAST [get_ports {DDR3_dq[4]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[4]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[3]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[3]}]
set_property SLEW FAST [get_ports {DDR3_dq[3]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[3]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[2]}]
set_property SLEW FAST [get_ports {DDR3_dq[2]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[2]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[1]}]
set_property SLEW FAST [get_ports {DDR3_dq[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[1]}]
set_property DIRECTION INOUT [get_ports {DDR3_dq[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[0]}]
set_property SLEW FAST [get_ports {DDR3_dq[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[0]}]
set_property DIRECTION OUT [get_ports {DDR3_odt[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_odt[0]}]
set_property SLEW FAST [get_ports {DDR3_odt[0]}]
set_property DIRECTION OUT [get_ports DDR3_cas_n]
set_property IOSTANDARD SSTL15 [get_ports DDR3_cas_n]
set_property SLEW FAST [get_ports DDR3_cas_n]
set_property DIRECTION OUT [get_ports DDR3_ras_n]
set_property IOSTANDARD SSTL15 [get_ports DDR3_ras_n]
set_property SLEW FAST [get_ports DDR3_ras_n]
set_property DIRECTION OUT [get_ports DDR3_reset_n]
set_property IOSTANDARD LVCMOS15 [get_ports DDR3_reset_n]
set_property DRIVE 12 [get_ports DDR3_reset_n]
set_property SLEW FAST [get_ports DDR3_reset_n]
set_property DIRECTION OUT [get_ports DDR3_we_n]
set_property IOSTANDARD SSTL15 [get_ports DDR3_we_n]
set_property SLEW FAST [get_ports DDR3_we_n]
#revert back to original instance
current_instance -quiet
