/** @file
 *  @brief MAVLink comm protocol generated from BTLink.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_BTLINK_H
#define MAVLINK_BTLINK_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_BTLINK.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{0, 254, 5, 5, 0, 0, 0}, {1, 7, 9, 9, 0, 0, 0}, {2, 119, 32, 32, 0, 0, 0}, {3, 215, 133, 133, 0, 0, 0}, {8, 65, 2, 2, 0, 0, 0}, {9, 173, 11, 11, 0, 0, 0}, {10, 34, 10, 10, 0, 0, 0}, {11, 48, 11, 11, 0, 0, 0}, {16, 75, 20, 20, 0, 0, 0}, {17, 235, 10, 10, 0, 0, 0}, {18, 39, 6, 6, 0, 0, 0}, {19, 208, 6, 6, 0, 0, 0}, {20, 197, 5, 5, 0, 0, 0}, {32, 33, 136, 136, 0, 0, 0}, {33, 82, 10, 10, 0, 0, 0}, {34, 195, 11, 11, 0, 0, 0}, {64, 224, 26, 26, 0, 0, 0}, {65, 241, 6, 6, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_BTLINK

// ENUM DEFINITIONS


/** @brief Состояния системы. */
#ifndef HAVE_ENUM_BT_STATE
#define HAVE_ENUM_BT_STATE
typedef enum BT_STATE
{
   STATE_BOOT=0, /* Система загружается. | */
   STATE_CALIBRATING=1, /* Система калибруется. | */
   STATE_STANDBY=2, /* Система в режиме ожидания | */
   STATE_ACTIVE=3, /* Система активна. | */
   STATE_CRITICAL=4, /* Система в критическом состоянии. | */
   STATE_ERROR=5, /* Система имеет ошибки. | */
   BT_STATE_ENUM_END=6, /*  | */
} BT_STATE;
#endif

/** @brief Error types. */
#ifndef HAVE_ENUM_BT_ERROR_TYPE
#define HAVE_ENUM_BT_ERROR_TYPE
typedef enum BT_ERROR_TYPE
{
   ERROR1=0, /*  | */
   BT_ERROR_TYPE_ENUM_END=1, /*  | */
} BT_ERROR_TYPE;
#endif

/** @brief Type of GPS fix */
#ifndef HAVE_ENUM_BT_GPS_FIX_TYPE
#define HAVE_ENUM_BT_GPS_FIX_TYPE
typedef enum BT_GPS_FIX_TYPE
{
   GPS_FIX_TYPE_NO_GPS=0, /* No GPS connected | */
   GPS_FIX_TYPE_NO_FIX=1, /* No position information, GPS is connected | */
   GPS_FIX_TYPE_2D_FIX=2, /* 2D position | */
   GPS_FIX_TYPE_3D_FIX=3, /* 3D position | */
   GPS_FIX_TYPE_DGPS=4, /* DGPS/SBAS aided 3D position | */
   GPS_FIX_TYPE_RTK_FLOAT=5, /* RTK float, 3D position | */
   GPS_FIX_TYPE_RTK_FIXED=6, /* RTK Fixed, 3D position | */
   GPS_FIX_TYPE_STATIC=7, /* Static fixed, typically used for base stations | */
   GPS_FIX_TYPE_PPP=8, /* PPP, 3D position. | */
   GPS_FIX_TYPE_MANUAL=9, /* Manual or fixed position. | */
   BT_GPS_FIX_TYPE_ENUM_END=10, /*  | */
} BT_GPS_FIX_TYPE;
#endif

/** @brief Типы данных параметров. */
#ifndef HAVE_ENUM_BT_PARAM_TYPE
#define HAVE_ENUM_BT_PARAM_TYPE
typedef enum BT_PARAM_TYPE
{
   PARAM_TYPE_UINT8=0, /* Целое беззнаковое число длинной 8 бит. | */
   PARAM_TYPE_INT8=1, /* Целое знаковое число длинной 8 бит. | */
   PARAM_TYPE_UINT16=2, /* Целое беззнаковое число длинной 16 бит. | */
   PARAM_TYPE_INT16=3, /* Целое знаковое число длинной 16 бит. | */
   PARAM_TYPE_UINT32=4, /* Целое беззнаковое число длинной 32 бита. | */
   PARAM_TYPE_INT32=5, /* Целое знаковое число длинной 32 бита. | */
   PARAM_TYPE_UINT64=6, /* Целое беззнаковое число длинной 64 бита. | */
   PARAM_TYPE_INT64=7, /* Целое знаковое число длинной 64 бита. | */
   PARAM_TYPE_REAL32=8, /* Число с плавующей точкой длинной 32 бита. | */
   PARAM_TYPE_REAL64=9, /* Число с плавующей точкой длинной 64 бита. | */
   BT_PARAM_TYPE_ENUM_END=10, /*  | */
} BT_PARAM_TYPE;
#endif

/** @brief Список параметров. */
#ifndef HAVE_ENUM_BT_PARAM
#define HAVE_ENUM_BT_PARAM
typedef enum BT_PARAM
{
   PARAM_DHCP=0, /* Использовать для подключения DHCP. | */
   PARAM_IP=1, /* IP адрес системы. | */
   PARAM_MASK=2, /* Маска подсети. | */
   PARAM_GATEWAY=3, /* Шлюз по умолчанию. | */
   PARAM_PORT=4, /* Номер порта для подключения. | */
   BT_PARAM_ENUM_END=5, /*  | */
} BT_PARAM;
#endif

/** @brief Результаты устанавки параметра. */
#ifndef HAVE_ENUM_BT_PARAM_ACK_TYPE
#define HAVE_ENUM_BT_PARAM_ACK_TYPE
typedef enum BT_PARAM_ACK_TYPE
{
   PARAM_ACK_ACCEPTED=0, /* Параметр установлен. | */
   PARAM_ACK_FAILED=1, /* Параметр отклонен. | */
   PARAM_ACK_PROGRESS=2, /* Параметр в процесе устанавки. | */
   BT_PARAM_ACK_TYPE_ENUM_END=3, /*  | */
} BT_PARAM_ACK_TYPE;
#endif

/** @brief Обрабатываемые команды. */
#ifndef HAVE_ENUM_BT_CMD_TYPE
#define HAVE_ENUM_BT_CMD_TYPE
typedef enum BT_CMD_TYPE
{
   RESET=0, /* Перезапуск системы. | */
   BT_CMD_TYPE_ENUM_END=1, /*  | */
} BT_CMD_TYPE;
#endif

/** @brief Результаты выполнения команды. */
#ifndef HAVE_ENUM_BT_CMD_ACK_TYPE
#define HAVE_ENUM_BT_CMD_ACK_TYPE
typedef enum BT_CMD_ACK_TYPE
{
   CMD_ACK_ACCEPTED=0, /* Команда выполнена. | */
   CMD_ACK_FAILED=1, /* Команда откланена. | */
   CMD_ACK_PROGRESS=2, /* Команда выполняется. | */
   BT_CMD_ACK_TYPE_ENUM_END=3, /*  | */
} BT_CMD_ACK_TYPE;
#endif

// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 3
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 3
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_heartbeat.h"
#include "./mavlink_msg_ping.h"
#include "./mavlink_msg_auth_key.h"
#include "./mavlink_msg_error.h"
#include "./mavlink_msg_param_get.h"
#include "./mavlink_msg_param_set.h"
#include "./mavlink_msg_param_value.h"
#include "./mavlink_msg_param_set_ack.h"
#include "./mavlink_msg_gps_raw.h"
#include "./mavlink_msg_temp_raw.h"
#include "./mavlink_msg_amplifier.h"
#include "./mavlink_msg_drive.h"
#include "./mavlink_msg_power.h"
#include "./mavlink_msg_raw_data.h"
#include "./mavlink_msg_target_data.h"
#include "./mavlink_msg_target_id_data.h"
#include "./mavlink_msg_cmd.h"
#include "./mavlink_msg_cmd_ack.h"

// base include


#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_HEARTBEAT, MAVLINK_MESSAGE_INFO_PING, MAVLINK_MESSAGE_INFO_AUTH_KEY, MAVLINK_MESSAGE_INFO_ERROR, MAVLINK_MESSAGE_INFO_PARAM_GET, MAVLINK_MESSAGE_INFO_PARAM_SET, MAVLINK_MESSAGE_INFO_PARAM_VALUE, MAVLINK_MESSAGE_INFO_PARAM_SET_ACK, MAVLINK_MESSAGE_INFO_GPS_RAW, MAVLINK_MESSAGE_INFO_TEMP_RAW, MAVLINK_MESSAGE_INFO_AMPLIFIER, MAVLINK_MESSAGE_INFO_DRIVE, MAVLINK_MESSAGE_INFO_POWER, MAVLINK_MESSAGE_INFO_RAW_DATA, MAVLINK_MESSAGE_INFO_TARGET_DATA, MAVLINK_MESSAGE_INFO_TARGET_ID_DATA, MAVLINK_MESSAGE_INFO_CMD, MAVLINK_MESSAGE_INFO_CMD_ACK}
# define MAVLINK_MESSAGE_NAMES {{ "AMPLIFIER", 18 }, { "AUTH_KEY", 2 }, { "CMD", 64 }, { "CMD_ACK", 65 }, { "DRIVE", 19 }, { "ERROR", 3 }, { "GPS_RAW", 16 }, { "HEARTBEAT", 0 }, { "PARAM_GET", 8 }, { "PARAM_SET", 9 }, { "PARAM_SET_ACK", 11 }, { "PARAM_VALUE", 10 }, { "PING", 1 }, { "POWER", 20 }, { "RAW_DATA", 32 }, { "TARGET_DATA", 33 }, { "TARGET_ID_DATA", 34 }, { "TEMP_RAW", 17 }}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_BTLINK_H
