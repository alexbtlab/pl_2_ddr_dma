#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import io
import socket
import time
import argparse
import logging
from threading import Thread
import BTLink as bt

def getArgs():
    parser = argparse.ArgumentParser(
        description='Тестовый скрипт для проверки системы обмена данными для МРЛС.')
    parser.add_argument(
        '-a', '--addr', type=str, default='127.0.0.1', help='IP адрес для подключения к МРЛС.')
    parser.add_argument(
        '-p', '--port', type=int, default=15550, help='Порт для подключения к МРЛС.')
    parser.add_argument(
        '-c', '--count', type=int, default=0, help='Количество отправляемых сообщений.')
    parser.add_argument(
        '-i', '--interval', type=int, default=0, help='Интервал между сообщениями.')
    return parser.parse_args()

class TestMavlink():
    def __init__(self, ip='127.0.0.1', port=15550):
        formatter = logging.Formatter('%(levelname)8s: %(message)s')
        handler = logging.StreamHandler(sys.stderr)
        handler.setFormatter(formatter)

        self.__logger = logging.Logger('test script')
        self.__logger.addHandler(handler)
        self.__logger.setLevel(logging.INFO)

        self.__ip = ip
        self.__port = port
        self.__addr = (self.__ip, self.__port)
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__mav = bt.MAVLink(io.BytesIO(), srcSystem=0)
        self.__mav.set_send_callback(self.write)
        self.__mav.set_callback(self.handler)

    def connect(self):
        self.__logger.info('Подключение...')
        try:
            self.__sock.connect(self.__addr)
            self.__logger.info('Подключение установлено.')
            return True
        except ConnectionRefusedError as err:
            self.__logger.error('Подключение неудалось.')
            self.__logger.error('Ошибка: {0}'.format(err))
            return False

    def read(self):
        while True:
            self.__logger.info('Прием данных.')
            try:
                data = self.__sock.recv(1024)
                if not data:
                    pass
                else:
                    self.__mav.parse_buffer(data)
            except socket.timeout:
                pass
            except Exception as err:
                self.__logger.error('Ошибка приема данных.')
                self.__logger.error('Ошибка: {0}'.format(err))

    def write(self, msg):
        buf = msg.pack(self.__mav)
        self.__sock.send(buf)

    def handler(self, msg):
        self.__logger.info('Принято сообщение: {0}'.format(msg))
        if msg.get_type() == 'HEARTBEAT':
            self.__logger.info('Сообщение содержит:')
            self.__logger.info('state: {0}'.format(msg.state))
            self.__logger.info('version: {0}'.format(msg.version))
        if msg.get_type() == 'CMD':
            pass
    
    def sendHeartbeat(self, state=0, version=0):
        self.__mav.heartbeat_send(state, version)

if __name__ == '__main__':
    args = getArgs()
    test = TestMavlink(args.addr, args.port)
    while not test.connect():
        pass
    Thread(target=test.read, daemon=True, name='Read thread').start()
    for i in range(args.count):
        test.sendHeartbeat(i, i*10)
        time.sleep(args.interval)
    #while True:
    #    pass
