#include "net_mrls.h"

static struct netif server_netif;
struct netif* echo_netif;
ip_addr_t ipaddr, netmask, gw;
u32 listenport;


void print_ip(char* msg, ip_addr_t* ip)
{
    print(msg);
    xil_printf("%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip),
        ip4_addr3(ip), ip4_addr4(ip));
}

void print_ip_settings(ip_addr_t* ip, ip_addr_t* mask, ip_addr_t* gw)
{
    print_ip("Board IP: ", ip);
    print_ip("Netmask : ", mask);
    print_ip("Gateway : ", gw);
}
void print_app_header()
{
    xil_printf("\n\r\n\r-----lwIP TCP echo server ------\n\r");
    xil_printf("TCP packets sent to port 6001 will be echoed back\n\r");
}
err_t recv_callback(void* arg, struct tcp_pcb* tpcb,
    struct pbuf* p, err_t err)
{
    /* do not read the packet if we are not in ESTABLISHED state */
    if (!p) {
        tcp_close(tpcb);
        tcp_recv(tpcb, NULL);
        return ERR_OK;
    }
    /*------------------------------------------------------------------------------------------------------------------*/
    /* indicate that the packet has been received */
    tcp_recved(tpcb, p->len);
    xil_printf("Receive data len: %d\n", p->len);
    parser((char*)p->payload, p->len); //Запуск парсера принятого пакета
    /*------------------------------------------------------------------------------------------------------------------*/
    int len;
    char bufTx[MAVLINK_MAX_PACKET_LEN]; // БУФЕР
    mavlink_message_t msgTx; // Сообщение
    
    mavlink_msg_heartbeat_pack(55, 2, &msgTx, 1, 2); // заполняем сообщение
    len = mavlink_msg_to_send_buffer((uint8_t*)bufTx, &msgTx); // преобразуем сообщение в буфер
    tcp_write(tpcb, bufTx, len, 1); // Запись на передачу
    
    mavlink_msg_target_id_data_pack(55, 2, &msgTx, 0, 55, 90, 500, 10);
    len = mavlink_msg_to_send_buffer((uint8_t*)bufTx, &msgTx); // преобразуем сообщение в буфер
    tcp_write(tpcb, bufTx, len, 1);

    // xil_printf("len-%d\n", p->len);
    // for (u32 i = 0; i < p->len; i++)
    //     XUartLite_SendByte(XPAR_AXI_UARTLITE_0_BASEADDR, *((char*)p->payload + i));

    /* echo back the payload */
    /* in this case, we assume that the payload is < TCP_SND_BUF */
    // if (tcp_sndbuf(tpcb) > p->len) {
    //     err = tcp_write(tpcb, p->payload, p->len, 1);
    // } else
    //     xil_printf("no space in tcp_sndbuf\n\r");

    /* free the received pbuf */
    pbuf_free(p);

    return ERR_OK;
}

err_t accept_callback(void* arg, struct tcp_pcb* newpcb, err_t err)
{
    static int connection = 1;

    /* set the receive callback for this connection */
    tcp_recv(newpcb, recv_callback);
    /* just use an integer number indicating the connection id as the
	   callback argument */
    tcp_arg(newpcb, (void*)(UINTPTR)connection);
    /* increment for subsequent accepted connections */
    connection++;
    return ERR_OK;
}

int start_application(u16 port)
{
    struct tcp_pcb* pcb;
    err_t err;
    //unsigned port = 7;

    /* create new TCP PCB structure */
    pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
    if (!pcb) {
        xil_printf("Error creating PCB. Out of Memory\n\r");
        return -1;
    }

    /* bind to specified @port */
    err = tcp_bind(pcb, IP_ANY_TYPE, port);
    if (err != ERR_OK) {
        xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
        return -2;
    }

    /* we do not need any arguments to callback functions */
    tcp_arg(pcb, NULL);

    /* listen for connections */
    pcb = tcp_listen(pcb);
    if (!pcb) {
        xil_printf("Out of memory while tcp_listen\n\r");
        return -3;
    }

    /* specify callback to use for incoming connections */
    tcp_accept(pcb, accept_callback);

    xil_printf("TCP echo server started @ port %d\n\r", port);

    return 0;
}

void ReadNetConfigFromSD(ip_addr_t* ipaddr, ip_addr_t* netmask, ip_addr_t* gw)
{

    /* the mac address of the board. this should be unique per board */
    unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };
    static char str_netconfSD[MAX_LEN_INI];
    echo_netif = &server_netif;
    u32 i = 0;

    DXSPISDVOL disk(XPAR_PMODSD_0_AXI_LITE_SPI_BASEADDR,
        XPAR_PMODSD_0_AXI_LITE_SDCS_BASEADDR);
    DFILE file;

    // The drive to mount the SD volume to.
    // Options are: "0:", "1:", "2:", "3:", "4:"
    static const char szDriveNbr[] = "0:";

    FRESULT fr;
    u32 bytesWritten = 0;
    u32 bytesRead, totalBytesRead;
    static char buff[128];
    static char* buffptr = buff;

    // Mount the disk
    DFATFS::fsmount(disk, szDriveNbr, 1);
    xil_printf("Disk mounted\r\n");

    fr = file.fsopen("netconfig.ini", FA_READ);
    if (fr == FR_OK) {
        xil_printf("Opened netconfig.ini\r\n");
        buffptr = buff;
        totalBytesRead = 0;
        do {
            fr = file.fsread((char*)buffptr, 1, &bytesRead);
            buffptr++;
            totalBytesRead += bytesRead; //xil_printf("'%s'\r\n", buff);
        } while ((totalBytesRead < MAX_LEN_INI) && fr == FR_OK);

        if (fr == FR_OK) {
            xil_printf("Read successful:\r\n");
            buff[totalBytesRead] = 0;
            xil_printf("'%s'\r\n", buff);
        } else {
            xil_printf("Read failed\r\n");
        }

        for (i = 0; i < MAX_LEN_INI; i++) {

            if (strncmp((buff + i), "ip:", 3) == 0)
                IP4_ADDR(ipaddr, atoi(buff + i + 3), atoi(buff + i + 7), atoi(buff + i + 11), atoi(buff + i + 15));
            if (strncmp((buff + i), "nm:", 3) == 0)
                IP4_ADDR(netmask, atoi(buff + i + 3), atoi(buff + i + 7), atoi(buff + i + 11), atoi(buff + i + 15));
            if (strncmp((buff + i), "gw:", 3) == 0)
                IP4_ADDR(gw, atoi(buff + i + 3), atoi(buff + i + 7), atoi(buff + i + 11), atoi(buff + i + 15));
            if (strncmp((buff + i), "port:", 5) == 0)
                listenport = atoi(buff + i + 5);
        }

    } else {
        xil_printf("Failed to open file to read from\r\n");
    }
}
void start_tcp_server(){

    ip_addr_t ipaddr, netmask, gw;
    /* the mac address of the board. this should be unique per board */
    unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };

    echo_netif = &server_netif;
    init_platform();
    ReadNetConfigFromSD(&ipaddr, &netmask, &gw); // Чтение конфигурации сети с SD карты
    print_app_header();
    lwip_init();

    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(echo_netif, &ipaddr, &netmask,
            &gw, mac_ethernet_address,
            PLATFORM_EMAC_BASEADDR)) {
        xil_printf("Error adding N/W interface\n\r");
    }

    netif_set_default(echo_netif);
    platform_enable_interrupts();
    netif_set_up(echo_netif);
    print_ip_settings(&ipaddr, &netmask, &gw);
    start_application(listenport);      
}
