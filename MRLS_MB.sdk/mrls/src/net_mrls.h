#ifndef SRC_NET_MRLS_H_
#define SRC_NET_MRLS_H_

#include "PmodSD.h"
#include "lwip/init.h"
#include "lwip/priv/tcp_priv.h"
#include "lwip/tcp.h"
#include "mavlink_parser/mavlink_parser.h"
#include "netif/xadapter.h"
#include "platform.h"
#include <stdio.h>
#include "platform_config.h"

#define MAX_LEN_INI 99

/* defined by each RAW mode application */
void print_app_header();
void tcp_fasttmr(void);
void tcp_slowtmr(void);
int start_application(u16);
int transfer_data();
void lwip_init();
void ReadNetConfigFromSD(ip_addr_t* ipaddr, ip_addr_t* netmask, ip_addr_t* gw);
void print_ip_settings(ip_addr_t* ip, ip_addr_t* mask, ip_addr_t* gw);
void start_tcp_server(void);

#endif /* SRC_NET_MRLS_H_ */
