#include "HMC769_v1.0.h"
#include "xparameters.h"

void HMC769_init()
{ //Функция инициализации IP ядра. Проверка идентификатора МС HMC769
    xil_printf("\r\n***Start init AXI_HMC769***\r\n");

    if (HMC769_read(HMC769_ID_REG) == 0x97370) // Проверка идентификатора МС
        xil_printf("INFO: initialize AXI_HMC769 complete \r\n");
    else
        xil_printf("ERROR: initialize AXI_HMC769 is fault\r\n");

    HMC769_write(HMC769_REFDIV_REG, 0x4); // Проверка работы интерфейса SPI. Запись значения 4 регистра делителя, затем чтение..

    if (HMC769_read(HMC769_REFDIV_REG) == 0x4) // Проверка записанного значения
        xil_printf("INFO: compare value is complete\r\n");
    else
        xil_printf("ERROR: compare value is fault\r\n");
}
void HMC769_setRegAdr(u8 adr)
{ //Функция установки адреса регистра МС HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* slv_reg1 = hmcbaseaddr + 1; // Адрес 1-го регистра для передачи данных в IP ядро из MB
    volatile unsigned int* slv_reg2 = hmcbaseaddr + 2; // Адрес 2-го регистра для передачи данных в IP ядро из MB
    // т.к. модуль SPI имеет 2 входа установки адреса (на ПРМ и ПРД)
    *slv_reg1 = adr; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg1 - адреса регистра МС для передачи)
    *slv_reg2 = adr; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_startSend()
{ // Функция формирования сигнала старта записи данных из МС HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    (*hmcbaseaddr) = 0x1;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_startReceive()
{ // Функция формирования сигнала старта чтения данных из МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    (*hmcbaseaddr) = 0x2;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_waitReady()
{ //Функция ожидания сигнала ready (окончания обмена)

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // Адрес 8-го регистра для приема данных из IP ядра в МС HMC769
    while ((*ip2mb_reg0) != 1) {
    }
}
u32 HMC769_getData()
{ // Функция чтения данных из МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* ip2mb_reg1 = hmcbaseaddr + 9; // Адрес 9-го регистра для приема данных из IP ядра в МС HMC769
    return *ip2mb_reg1;
}
void HMC769_setSendSata(u32 dataSend)
{ // Функция формирования данных для передачу в МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* slv_reg3 = hmcbaseaddr + 3; // Адрес 3-го регистра для передачи данных в IP ядро из MB
    *slv_reg3 = dataSend;
}
void HMC769_write(u8 adrRegHMC, u32 dataSend)
{ // Функция записи данных u32 dataSend по адресу u8 adrRegH в МС HMC769

    HMC769_setRegAdr(adrRegHMC); // установка адреса регистра МС HMC769
    HMC769_setSendSata(dataSend); // Формирование данных для передачу в МС HMC769
    HMC769_startSend(); // Формирование сигнала старта записи данных из МС HMC769
    HMC769_waitReady(); // Ожидание сигнала ready (окончания обмена)
}
u32 HMC769_read(u8 adrRegHMC)
{ // Функция чтения данных u32 по адресу u8 adrRegH из МС HMC769

    HMC769_setRegAdr(adrRegHMC); // установка адреса регистра МС HMC769
    HMC769_startReceive(); // Формирование сигнала старта чтения данных из МС HMC769
    HMC769_waitReady(); // Ожидание сигнала ready (окончания обмена)
    return HMC769_getData(); // Чтение данных из МС HMC769
}
void HMC769_readBitMap(u8 adrRegHMC, void* rxReg)
{ // Функция чтения битовой карты регистра adrRegHMC МС HMC769
    u32 dataReg;
    rxReg = &dataReg; // Указываем регистр dataReg принятых данных от МС, на необходимую структуру (void*)(созданную во время входа в функцию) 
                      // т.к. на каждый регистр принятых данных от МС есть своя структура   

    dataReg = HMC769_read(adrRegHMC); // Чтение данных по адресу u8 adrRegH из МС HMC769
    xil_printf("\r\n***HMC769_readBitMap***   ADR_REG:%x DATA_REG:%x\r\n", adrRegHMC, *(u32*)rxReg);

    switch (adrRegHMC) { 
    case HMC769_ID_REG:
    	xil_printf("chip_ID-%x\r\n", ((t_HMC769_ID_REG*)rxReg)->chip_ID);
        break;
    case HMC769_RST_Register_REG:
        xil_printf("EnFromSPI-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnFromSPI);
        xil_printf("EnKeepOns-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnKeepOns);
        xil_printf("EnPinSel-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnPinSel);
        xil_printf("EnSyncChpDis-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnSyncChpDis);
        break;
    case HMC769_REFDIV_REG:
        xil_printf("rdiv:%x\r\n", ((t_HMC769_REFDIV_REG*)rxReg)->rdiv);
        break;
    case HMC769_Frequency_Register_REG:
        xil_printf("intg-%x\r\n", ((t_HMC769_Frequency_Register_REG*)rxReg)->intg);
        break;
    case HMC769_Frequency_Register_Fractional_Part_REG:
        xil_printf("frac-%x\r\n", ((t_HMC769_Frequency_Register_Fractional_Part_REG*)rxReg)->frac);
        break;
    case HMC769_Seed_REG:
        xil_printf("SEED:%x\r\n", ((t_HMC769_Seed_REG*)rxReg)->SEED);
        break;
    case HMC769_SD_CFG_REG:
        xil_printf("autoseed:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->autoseed);
        xil_printf("BIST_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->BIST_Enable);
        xil_printf("Disable_Reset:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Disable_Reset);
        xil_printf("DSM_Clock_Source:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->DSM_Clock_Source);
        xil_printf("External_Trigger_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->External_Trigger_Enable);
        xil_printf("Force_DSM_Clock_n:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_DSM_Clock_n);
        xil_printf("Force_RDIV_bypass:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_RDIV_bypass);
        xil_printf("Invert_DSM_Clock:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Invert_DSM_Clock);
        xil_printf("Modulator_Type:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Modulator_Type);
        xil_printf("Number_of_Bist_Cycles:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Number_of_Bist_Cycles);
        xil_printf("Reserved_0:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_0);
        xil_printf("Reserved_1:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_1);
        xil_printf("Reserved_7:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_7);
        xil_printf("SD_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->SD_Mode);
        xil_printf("Single_Step_Ramp_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Single_Step_Ramp_Mode);
        break;
    case HMC769_Lock_Detect_REG:
        xil_printf("Cycle_Slip_Prevention_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Cycle_Slip_Prevention_Enable);
        xil_printf("LKDCounts:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LKDCounts);
        xil_printf("Lock_Detect_Timer_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Lock_Detect_Timer_Enable);
        xil_printf("LockDetect_Counters_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LockDetect_Counters_Enable);
        xil_printf("Train_Lock_Detect_Timer:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Train_Lock_Detect_Timer);
        break;
    case HMC769_Analog_EN_REG:
        xil_printf("EnBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnBias);
        xil_printf("EnCP:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnCP);
        xil_printf("EnMcnt:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnMcnt);
        xil_printf("EnOpAmp:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnOpAmp);
        xil_printf("EnPFD:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPFD);
        xil_printf("EnPS:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPS);
        xil_printf("EnVCO:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCO);
        xil_printf("EnVCOBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCOBias);
        xil_printf("EnXtal:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnXtal);
        xil_printf("RFDiv2Sel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->RFDiv2Sel);
        xil_printf("VCOBWSel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOBWSel);
        xil_printf("VCOOutBiasA:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasA);
        xil_printf("VCOOutBiasB:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasB);
        xil_printf("XtalDisSat:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalDisSat);
        xil_printf("XtalLowGain:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalLowGain);
        break;
    case HMC769_Charge_Pump_REG:
        xil_printf("CPHiK:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPHiK);
        xil_printf("CPIdn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIdn);
        xil_printf("CPIup:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIup);
        xil_printf("CPOffset:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPOffset);
        xil_printf("CPSnkEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSnkEn);
        xil_printf("CPSrcEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSrcEn);
        break;
    case HMC769_Modulation_Step_REG:
        xil_printf("MODSTEP:%x\r\n", ((t_HMC769_Modulation_Step_REG*)rxReg)->MODSTEP);
        break;
    case HMC769_PD_REG:
        xil_printf("LKDProcTesttoCP:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->LKDProcTesttoCP);
        xil_printf("McntClkGateSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->McntClkGateSel);
        xil_printf("PFDDly:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDly);
        xil_printf("PFDDnEN:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDnEN);
        xil_printf("PFDForceDn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceDn);
        xil_printf("PFDForceMid:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceMid);
        xil_printf("PFDForceUp:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceUp);
        xil_printf("PFDInv:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDInv);
        xil_printf("PFDShort:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDShort);
        xil_printf("PFDUpEn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDUpEn);
        xil_printf("PSBiasSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PSBiasSel);
        xil_printf("VDIVExt:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->VDIVExt);
        break;
    case HMC769_ALTINT_REG:
        xil_printf("ALTINT:%x\r\n", ((t_HMC769_ALTINT_REG*)rxReg)->ALTINT);
        break;
    case HMC769_ALTFRAC_REG:
        xil_printf("ALTFRAC:%x\r\n", ((t_HMC769_ALTFRAC_REG*)rxReg)->ALTFRAC);
        break;
    case HMC769_SPI_TRIG_REG:
        xil_printf("SPITRIG:%x\r\n", ((t_HMC769_SPI_TRIG_REG*)rxReg)->SPITRIG);
        break;
    case HMC769_GPO_REG:
        xil_printf("GPOAlways:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOAlways);
        xil_printf("GPOOn:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOOn);
        xil_printf("GPOPullDnDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullDnDis);
        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf("GPOTest:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOTest);
        break;
    case HMC769_GPO2_REG:
        xil_printf("GPO:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->GPO);
        xil_printf("Lock_Detect:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Lock_Detect);
        xil_printf("Ramp_Busy:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Ramp_Busy);
        break;
    case HMC769_BIST_REG:
        xil_printf("BIST_Busy:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Busy);
        xil_printf("BIST_Signature:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Signature);
        break;
    case HMC769_Lock_Detect_Timer_Status_REG:
        xil_printf("LkdSpeed:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdSpeed);
        xil_printf("LkdTraining:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdTraining);
        break;

    default:
        xil_printf("ERROR: no register with the given address u8 adrRegHMC\r\n");
        break;
    }
}
void HMC769_configIC()
{ // Функция конфигурации МС HMC769
    /*
    static t_HMC769_SD_CFG_REG* sdCfgReg;
    sdCfgReg = &dataReg; */

    /*---WRITE HMC769_SD_CFG_REG REGISTER DATA---*/
    /*  sdCfgReg->autoseed = 0;
    sdCfgReg->BIST_Enable = 0;
    sdCfgReg->Disable_Reset = 0;
    HMC769_write(HMC769_SD_CFG_REG, dataReg); */

    //u32 dataReg;
    //dataReg = HMC769_read(2);
    //xil_printf("INFO:   dataReg-%x\r\n", dataReg);
    //xil_printf("INFO:   rdiv-%x\r\n", dataReg & rdiv);
}
void HMC769_viewAllDataPLL()
{ // Функция чтения карты регистров МС HMC769
    static t_HMC769_ID_REG* idReg;
    static t_HMC769_RST_Register_REG* rstReg;
    static t_HMC769_REFDIV_REG* refdivReg;
    static t_HMC769_Frequency_Register_REG* freqReg;
    static t_HMC769_Frequency_Register_Fractional_Part_REG* freqFracReg;
    static t_HMC769_Seed_REG* seedReg;
    static t_HMC769_SD_CFG_REG* sdCfgReg;
    static t_HMC769_Lock_Detect_REG* lockDetReg;
    static t_HMC769_Analog_EN_REG* AnEnReg;
    static t_HMC769_Charge_Pump_REG* ChargePumpReg;
    static t_HMC769_Modulation_Step_REG* modStepReg;
    static t_HMC769_PD_REG* pdReg;
    static t_HMC769_ALTINT_REG* altintReg;
    static t_HMC769_ALTFRAC_REG* altFracReg;
    static t_HMC769_SPI_TRIG_REG* spiTrigReg;
    static t_HMC769_GPO_REG* gpoReg;
    static t_HMC769_GPO2_REG* gpo2Reg;
    static t_HMC769_BIST_REG* bistReg;
    static t_HMC769_Lock_Detect_Timer_Status_REG* lockDetTimStatReg;

    HMC769_readBitMap(HMC769_ID_REG, idReg);
    HMC769_readBitMap(HMC769_RST_Register_REG, rstReg);
    HMC769_readBitMap(HMC769_REFDIV_REG, refdivReg);
    HMC769_readBitMap(HMC769_Frequency_Register_REG, freqReg);
    HMC769_readBitMap(HMC769_Frequency_Register_Fractional_Part_REG, freqFracReg);
    HMC769_readBitMap(HMC769_Seed_REG, seedReg);
    HMC769_readBitMap(HMC769_SD_CFG_REG, sdCfgReg);
    HMC769_readBitMap(HMC769_Lock_Detect_REG, lockDetReg);
    HMC769_readBitMap(HMC769_Analog_EN_REG, AnEnReg);
    HMC769_readBitMap(HMC769_Charge_Pump_REG, ChargePumpReg);
    HMC769_readBitMap(HMC769_Modulation_Step_REG, modStepReg);
    HMC769_readBitMap(HMC769_PD_REG, pdReg);
    HMC769_readBitMap(HMC769_ALTINT_REG, altintReg);
    HMC769_readBitMap(HMC769_ALTFRAC_REG, altFracReg);
    HMC769_readBitMap(HMC769_SPI_TRIG_REG, spiTrigReg);
    HMC769_readBitMap(HMC769_GPO_REG, gpoReg);
    HMC769_readBitMap(HMC769_GPO2_REG, gpo2Reg);
    HMC769_readBitMap(HMC769_BIST_REG, bistReg);
    HMC769_readBitMap(HMC769_Lock_Detect_Timer_Status_REG, lockDetTimStatReg);
}
