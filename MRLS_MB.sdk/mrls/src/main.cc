#include "HMC769.h"
#include "net_mrls.h"
//#include "xscugic.h"
#include "xaxidma.h"
#include "xintc.h"

#include "xil_printf.h"

 #define INTC		XIntc
extern struct netif* echo_netif;
u32 globalFrameCounter = 0;
XIntc InterruptController;
XAxiDma 		Dma;
u32 *heap;

int Enable_sampleGen(unsigned int numOfWord);
void InterruptHandler();
int InitializeInterruptSystem();
void InitializeAxiDMA( );
void startDmaTransfer(unsigned int dstAddr, unsigned int len);
static int SetupInterruptSystem(INTC * IntcInstancePtr,  XAxiDma * AxiDmaPtr, u16 TxIntrId, u16 RxIntrId);


#define HEAP_SIZE		0x800
#define INTC_DEVICE_ID          XPAR_INTC_0_DEVICE_ID
#define INTC_HANDLER	XIntc_InterruptHandler
#define RESET_TIMEOUT_COUNTER	10000

#define RX_INTR_ID		XPAR_AXI_INTC_0_AXI_DMA_0_S2MM_INTROUT_INTR
#define TX_INTR_ID		XPAR_AXI_INTC_0_AXI_DMA_0_S2MM_INTROUT_INTR
#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID
#define NUMBER_OF_TRANSFERS	10

#define MEM_BASE_ADDR		0x01000000
#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x004FFFFF)

static void TxIntrHandler(void *Callback);
static void RxIntrHandler(void *Callback);


volatile int TxDone;
volatile int RxDone;
volatile int Error;

int Enable_sampleGen(unsigned int numOfWord){

    Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR, numOfWord);
    Xil_Out32(XPAR_AXI_GPIO_1_BASEADDR, 1);
    return 0;
}
void InterruptHandler(){
    u32 tmpVal;

    tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x34);
    tmpVal = tmpVal | 0x1000;
    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x34, tmpVal);
	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst;

	/* Read pending interrupts */
	//IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DMA_TO_DEVICE);

	/* Acknowledge pending interrupts */
	xil_printf("INFO: int\r\n");

	//XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DMA_TO_DEVICE);

    //globalFrameCounter++;
    // if (globalFrameCounter > 10000) {
    //    xil_printf("INFO: frame counter:%x\r\n", globalFrameCounter);
    ////    return;
    //}
    //startDmaTransfer(0x80020000, 256);
}
static void Handler(void *Callback) {
	u32 pending;

	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;

	// Read and acknowledge interrupt
	pending = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrAckIrq(AxiDmaInst, pending, XAXIDMA_DEVICE_TO_DMA);

	xil_printf("\n\nINTERRUPT HAPPENING!\n\n");
	heap[0] = -1;
	heap[77] = -2;
	return;
}
static int SetupInterruptSystem(INTC * IntcInstancePtr,  XAxiDma * AxiDmaPtr, u16 TxIntrId, u16 RxIntrId)
{
	 int Status;


	/* Initialize the interrupt controller and connect the ISRs */
	Status = XIntc_Initialize(IntcInstancePtr, INTC_DEVICE_ID);
	if (Status != XST_SUCCESS) {

		xil_printf("Failed init intc\r\n");
		return XST_FAILURE;
	}

	Status = XIntc_Connect(IntcInstancePtr, TxIntrId,
			       (XInterruptHandler) TxIntrHandler, AxiDmaPtr);
	if (Status != XST_SUCCESS) {

		xil_printf("Failed tx connect intc\r\n");
		return XST_FAILURE;
	}

	Status = XIntc_Connect(IntcInstancePtr, RxIntrId,
			       (XInterruptHandler) RxIntrHandler, AxiDmaPtr);
	if (Status != XST_SUCCESS) {

		xil_printf("Failed rx connect intc\r\n");
		return XST_FAILURE;
	}

	/* Start the interrupt controller */
	Status = XIntc_Start(IntcInstancePtr, XIN_REAL_MODE);
	if (Status != XST_SUCCESS) {

		xil_printf("Failed to start intc\r\n");
		return XST_FAILURE;
	}

	XIntc_Enable(IntcInstancePtr, TxIntrId);
	XIntc_Enable(IntcInstancePtr, RxIntrId);

	/* Enable interrupts from the hardware */

	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler)INTC_HANDLER,
			(void *)IntcInstancePtr);

	Xil_ExceptionEnable();

	return XST_SUCCESS;
}
int InitializeInterruptSystem(XAxiDma *dma)
{

	int Status;
	XAxiDma_Config *Config;
	int Tries = NUMBER_OF_TRANSFERS;
	int Index;
	u8 *TxBufferPtr;
	u8 *RxBufferPtr;
	u8 Value;

	TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
	RxBufferPtr = (u8 *)RX_BUFFER_BASE;
	/* Initial setup for Uart16550 */
#ifdef XPAR_UARTNS550_0_BASEADDR

	Uart550_Setup();

#endif

	xil_printf("\r\n--- Entering main() --- \r\n");

	Config = XAxiDma_LookupConfig(DMA_DEV_ID);
	if (!Config) {
		xil_printf("No config found for %d\r\n", DMA_DEV_ID);

		return XST_FAILURE;
	}


	/* Enable all interrupts */
	XAxiDma_IntrEnable(dma, XAXIDMA_IRQ_ALL_MASK,
							XAXIDMA_DMA_TO_DEVICE);


	XAxiDma_IntrEnable(dma, XAXIDMA_IRQ_ALL_MASK,
							XAXIDMA_DEVICE_TO_DMA);

		/*
		 * Initialize the interrupt controller driver so that it is ready to
		 * use.
		 */
		Status = XIntc_Initialize(&InterruptController, XPAR_INTC_0_DEVICE_ID);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		/*
		 * Connect a device driver handler that will be called when an interrupt
		 * for the device occurs, the device driver handler performs the
		 * specific interrupt processing for the device.
		 */
		Status = XIntc_Connect(&InterruptController, XPAR_INTC_0_AXIDMA_0_VEC_ID,
					       (XInterruptHandler) InterruptHandler, (void *)dma);
			if (Status != XST_SUCCESS) {
				xil_printf("Failed to bind interrupt handler\r\n");
				return XST_FAILURE;
			}
		/*
		 * Start the interrupt controller such that interrupts are enabled for
		 * all devices that cause interrupts, specific real mode so that
		 * the UartLite can cause interrupts through the interrupt controller.
		 */
		Status = XIntc_Start(&InterruptController, XIN_REAL_MODE);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		/*
		 * Enable the interrupt for the UartLite device.
		 */
		XIntc_Enable(&InterruptController, XPAR_INTC_0_AXIDMA_0_VEC_ID);
		/*
		 * Initialize the exception table.
		 */
		Xil_ExceptionInit();
		/*
		 * Register the interrupt controller handler with the exception table.
		 */
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				 (Xil_ExceptionHandler)XIntc_InterruptHandler,
				 &InterruptController);

		/*
		 * Enable exceptions.
		 */
		Xil_ExceptionEnable();

		return XST_SUCCESS;
}
void InitializeAxiDMA( ){
    unsigned int tmpVal;

    tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x30);
    tmpVal |= 0x1001;
    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + 0x30, tmpVal);
    tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + 0x30);
    xil_printf("\r\nINFO: read DMA control register:%x\r\n", tmpVal);
}
void startDmaTransfer(unsigned int dstAddr, unsigned int len){

    Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x48, dstAddr);
    Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x58, len);
}
int main()
{

	//int * c = (int *)0x
	//u32 * tmpadr;
	//heap = (u32*)malloc(HEAP_SIZE);
    //start_tcp_server();       // Чтение конфигурации сети с SD карты и запуск сервера
    //HMC769_init();            // Инициализация IP ядра конфигурации МС HMC769
    //HMC769_viewAllDataPLL();  // Отображение карты регистров МС HMC769
    InitializeAxiDMA();
    Enable_sampleGen(32);
    InitializeInterruptSystem (&Dma);
	startDmaTransfer(0x80020000, 256);
    /* receive and process packets */

	xil_printf("\r\nINFO: adr b:\r\n");
    while (1) {

        //    xemacif_input(echo_netif);
        //    transfer_data();
    }
    /* never reached */
    //cleanup_platform();
    return 0;
}
/*****************************************************************************/
/*
*
* This is the DMA TX Interrupt handler function.
*
* It gets the interrupt status from the hardware, acknowledges it, and if any
* error happens, it resets the hardware. Otherwise, if a completion interrupt
* is present, then sets the TxDone.flag
*
* @param	Callback is a pointer to TX channel of the DMA engine.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void TxIntrHandler(void *Callback)
{

	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;

	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DMA_TO_DEVICE);

	/* Acknowledge pending interrupts */


	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DMA_TO_DEVICE);

	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {

		return;
	}

	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {

		Error = 1;

		/*
		 * Reset should never fail for transmit channel
		 */
		XAxiDma_Reset(AxiDmaInst);

		TimeOut = RESET_TIMEOUT_COUNTER;

		while (TimeOut) {
			if (XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}

			TimeOut -= 1;
		}

		return;
	}

	/*
	 * If Completion interrupt is asserted, then set the TxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {

		TxDone = 1;
	}
}

/*****************************************************************************/
/*
*
* This is the DMA RX interrupt handler function
*
* It gets the interrupt status from the hardware, acknowledges it, and if any
* error happens, it resets the hardware. Otherwise, if a completion interrupt
* is present, then it sets the RxDone flag.
*
* @param	Callback is a pointer to RX channel of the DMA engine.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void RxIntrHandler(void *Callback)
{
	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;

	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DEVICE_TO_DMA);

	/* Acknowledge pending interrupts */
	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DEVICE_TO_DMA);

	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {
		return;
	}

	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {

		Error = 1;

		/* Reset could fail and hang
		 * NEED a way to handle this or do not call it??
		 */
		XAxiDma_Reset(AxiDmaInst);

		TimeOut = RESET_TIMEOUT_COUNTER;

		while (TimeOut) {
			if(XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}

			TimeOut -= 1;
		}

		return;
	}

	/*
	 * If completion interrupt is asserted, then set RxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {

		RxDone = 1;
	}
}
