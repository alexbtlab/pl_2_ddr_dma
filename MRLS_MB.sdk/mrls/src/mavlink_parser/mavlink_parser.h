#include "mavlink.h"

#define CURRENT_SYSID 5



extern void parser(const char* buffer, size_t len);
extern void handler(mavlink_message_t* message);
extern void handler_heartbeat(mavlink_message_t* message);
extern void handler_param_get(mavlink_message_t* message);
extern void handler_param_set(mavlink_message_t* message);
extern void handler_cmd(mavlink_message_t* message);
