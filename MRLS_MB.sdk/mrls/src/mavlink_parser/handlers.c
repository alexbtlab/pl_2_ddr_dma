#include "mavlink_parser.h"

void handler(mavlink_message_t* message)
{
    switch (message->msgid) {
    case MAVLINK_MSG_ID_HEARTBEAT:
        handler_heartbeat(message);
        break;
    case MAVLINK_MSG_ID_PARAM_GET:
        handler_param_get(message);
        break;
    case MAVLINK_MSG_ID_PARAM_SET:
        handler_param_set(message);
        break;
    case MAVLINK_MSG_ID_CMD:
        handler_cmd(message);
        break;
    default:
        break;
    }
}

void handler_heartbeat(mavlink_message_t* message)
{
    // Декодируем сообщение
    mavlink_heartbeat_t heartbeat;
    mavlink_msg_heartbeat_decode(message, &heartbeat);
    // TODO Здесь обрабатывается heartbeat
}

void handler_param_get(mavlink_message_t* message)
{
    // Декодируем сообщение
    mavlink_param_get_t param_get;
    mavlink_msg_param_get_decode(message, &param_get);
    // Проверяем нам ли это сообщение
    if (param_get.target != CURRENT_SYSID)
        return;
    // TODO Доработать switch под все параметры
    switch (param_get.param) {
    case PARAM_DHCP:
        // TODO Здесь надо отправить информацию об использовании DHCP
        break;
    case PARAM_IP:
        // TODO Здесь надо отправить текущий IP адрес
        break;
    case PARAM_GATEWAY:
        // TODO Здесь надо отправить текущий шлюз
        break;
    case PARAM_MASK:
        // TODO Здесь надо отправить текущую маску подсети
        break;
    case PARAM_PORT:
        // TODO Здесь надо отправить текущий порт
        break;
    }
}

void handler_param_set(mavlink_message_t* message)
{
    // Декодируем сообщение
    mavlink_param_set_t param_set;
    mavlink_msg_param_set_decode(message, &param_set);
    // Проверяем нам ли это сообщение
    if (param_set.target != CURRENT_SYSID)
        return;
    // TODO Доработать switch под все параметры
    switch (param_set.param) {
    case PARAM_DHCP:
        // TODO Здесь надо отправить информацию об использовании DHCP
        break;
    case PARAM_IP:
        // TODO Здесь надо отправить текущий IP адрес
        break;
    case PARAM_GATEWAY:
        // TODO Здесь надо отправить текущий шлюз
        break;
    case PARAM_MASK:
        // TODO Здесь надо отправить текущую маску подсети
        break;
    case PARAM_PORT:
        // TODO Здесь надо отправить текущий порт
        break;
    }
}

void handler_cmd(mavlink_message_t* message)
{
    // Декодируем сообщение
    mavlink_cmd_t cmd;
    mavlink_msg_cmd_decode(message, &cmd);
    // Проверяем нам ли это сообщение
    if (cmd.target != CURRENT_SYSID)
        return;
    // TODO Доработать switch под все команды
    switch (cmd.cmd) {
    case RESET:
        // TODO Здесь перезагружаем устройство
        break;
    }
}
