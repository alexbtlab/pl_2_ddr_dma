#pragma once
// MESSAGE RAW_DATA PACKING

#define MAVLINK_MSG_ID_RAW_DATA 32

MAVPACKED(
typedef struct __mavlink_raw_data_t {
 uint32_t time; /*<  Время в формате UNIX или время от запуска системы.*/
 uint16_t azimuth; /*<  Азимут.*/
 int16_t data[64]; /*<  Данные.*/
 int8_t count; /*<  Количество пакеов в наборе.*/
 int8_t index; /*<  Номер пакета в наборе.*/
}) mavlink_raw_data_t;

#define MAVLINK_MSG_ID_RAW_DATA_LEN 136
#define MAVLINK_MSG_ID_RAW_DATA_MIN_LEN 136
#define MAVLINK_MSG_ID_32_LEN 136
#define MAVLINK_MSG_ID_32_MIN_LEN 136

#define MAVLINK_MSG_ID_RAW_DATA_CRC 33
#define MAVLINK_MSG_ID_32_CRC 33

#define MAVLINK_MSG_RAW_DATA_FIELD_DATA_LEN 64

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_RAW_DATA { \
    32, \
    "RAW_DATA", \
    5, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_raw_data_t, time) }, \
         { "azimuth", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_raw_data_t, azimuth) }, \
         { "count", NULL, MAVLINK_TYPE_INT8_T, 0, 134, offsetof(mavlink_raw_data_t, count) }, \
         { "index", NULL, MAVLINK_TYPE_INT8_T, 0, 135, offsetof(mavlink_raw_data_t, index) }, \
         { "data", NULL, MAVLINK_TYPE_INT16_T, 64, 6, offsetof(mavlink_raw_data_t, data) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_RAW_DATA { \
    "RAW_DATA", \
    5, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_raw_data_t, time) }, \
         { "azimuth", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_raw_data_t, azimuth) }, \
         { "count", NULL, MAVLINK_TYPE_INT8_T, 0, 134, offsetof(mavlink_raw_data_t, count) }, \
         { "index", NULL, MAVLINK_TYPE_INT8_T, 0, 135, offsetof(mavlink_raw_data_t, index) }, \
         { "data", NULL, MAVLINK_TYPE_INT16_T, 64, 6, offsetof(mavlink_raw_data_t, data) }, \
         } \
}
#endif

/**
 * @brief Pack a raw_data message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Время в формате UNIX или время от запуска системы.
 * @param azimuth  Азимут.
 * @param count  Количество пакеов в наборе.
 * @param index  Номер пакета в наборе.
 * @param data  Данные.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_raw_data_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint16_t azimuth, int8_t count, int8_t index, const int16_t *data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RAW_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, azimuth);
    _mav_put_int8_t(buf, 134, count);
    _mav_put_int8_t(buf, 135, index);
    _mav_put_int16_t_array(buf, 6, data, 64);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_RAW_DATA_LEN);
#else
    mavlink_raw_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.count = count;
    packet.index = index;
    mav_array_memcpy(packet.data, data, sizeof(int16_t)*64);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_RAW_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_RAW_DATA;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
}

/**
 * @brief Pack a raw_data message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Время в формате UNIX или время от запуска системы.
 * @param azimuth  Азимут.
 * @param count  Количество пакеов в наборе.
 * @param index  Номер пакета в наборе.
 * @param data  Данные.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_raw_data_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint16_t azimuth,int8_t count,int8_t index,const int16_t *data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RAW_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, azimuth);
    _mav_put_int8_t(buf, 134, count);
    _mav_put_int8_t(buf, 135, index);
    _mav_put_int16_t_array(buf, 6, data, 64);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_RAW_DATA_LEN);
#else
    mavlink_raw_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.count = count;
    packet.index = index;
    mav_array_memcpy(packet.data, data, sizeof(int16_t)*64);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_RAW_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_RAW_DATA;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
}

/**
 * @brief Encode a raw_data struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param raw_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_raw_data_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_raw_data_t* raw_data)
{
    return mavlink_msg_raw_data_pack(system_id, component_id, msg, raw_data->time, raw_data->azimuth, raw_data->count, raw_data->index, raw_data->data);
}

/**
 * @brief Encode a raw_data struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param raw_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_raw_data_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_raw_data_t* raw_data)
{
    return mavlink_msg_raw_data_pack_chan(system_id, component_id, chan, msg, raw_data->time, raw_data->azimuth, raw_data->count, raw_data->index, raw_data->data);
}

/**
 * @brief Send a raw_data message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Время в формате UNIX или время от запуска системы.
 * @param azimuth  Азимут.
 * @param count  Количество пакеов в наборе.
 * @param index  Номер пакета в наборе.
 * @param data  Данные.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_raw_data_send(mavlink_channel_t chan, uint32_t time, uint16_t azimuth, int8_t count, int8_t index, const int16_t *data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RAW_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, azimuth);
    _mav_put_int8_t(buf, 134, count);
    _mav_put_int8_t(buf, 135, index);
    _mav_put_int16_t_array(buf, 6, data, 64);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RAW_DATA, buf, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
#else
    mavlink_raw_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.count = count;
    packet.index = index;
    mav_array_memcpy(packet.data, data, sizeof(int16_t)*64);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RAW_DATA, (const char *)&packet, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
#endif
}

/**
 * @brief Send a raw_data message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_raw_data_send_struct(mavlink_channel_t chan, const mavlink_raw_data_t* raw_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_raw_data_send(chan, raw_data->time, raw_data->azimuth, raw_data->count, raw_data->index, raw_data->data);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RAW_DATA, (const char *)raw_data, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
#endif
}

#if MAVLINK_MSG_ID_RAW_DATA_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_raw_data_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint16_t azimuth, int8_t count, int8_t index, const int16_t *data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint16_t(buf, 4, azimuth);
    _mav_put_int8_t(buf, 134, count);
    _mav_put_int8_t(buf, 135, index);
    _mav_put_int16_t_array(buf, 6, data, 64);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RAW_DATA, buf, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
#else
    mavlink_raw_data_t *packet = (mavlink_raw_data_t *)msgbuf;
    packet->time = time;
    packet->azimuth = azimuth;
    packet->count = count;
    packet->index = index;
    mav_array_memcpy(packet->data, data, sizeof(int16_t)*64);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RAW_DATA, (const char *)packet, MAVLINK_MSG_ID_RAW_DATA_MIN_LEN, MAVLINK_MSG_ID_RAW_DATA_LEN, MAVLINK_MSG_ID_RAW_DATA_CRC);
#endif
}
#endif

#endif

// MESSAGE RAW_DATA UNPACKING


/**
 * @brief Get field time from raw_data message
 *
 * @return  Время в формате UNIX или время от запуска системы.
 */
static inline uint32_t mavlink_msg_raw_data_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field azimuth from raw_data message
 *
 * @return  Азимут.
 */
static inline uint16_t mavlink_msg_raw_data_get_azimuth(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Get field count from raw_data message
 *
 * @return  Количество пакеов в наборе.
 */
static inline int8_t mavlink_msg_raw_data_get_count(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  134);
}

/**
 * @brief Get field index from raw_data message
 *
 * @return  Номер пакета в наборе.
 */
static inline int8_t mavlink_msg_raw_data_get_index(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  135);
}

/**
 * @brief Get field data from raw_data message
 *
 * @return  Данные.
 */
static inline uint16_t mavlink_msg_raw_data_get_data(const mavlink_message_t* msg, int16_t *data)
{
    return _MAV_RETURN_int16_t_array(msg, data, 64,  6);
}

/**
 * @brief Decode a raw_data message into a struct
 *
 * @param msg The message to decode
 * @param raw_data C-struct to decode the message contents into
 */
static inline void mavlink_msg_raw_data_decode(const mavlink_message_t* msg, mavlink_raw_data_t* raw_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    raw_data->time = mavlink_msg_raw_data_get_time(msg);
    raw_data->azimuth = mavlink_msg_raw_data_get_azimuth(msg);
    mavlink_msg_raw_data_get_data(msg, raw_data->data);
    raw_data->count = mavlink_msg_raw_data_get_count(msg);
    raw_data->index = mavlink_msg_raw_data_get_index(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_RAW_DATA_LEN? msg->len : MAVLINK_MSG_ID_RAW_DATA_LEN;
        memset(raw_data, 0, MAVLINK_MSG_ID_RAW_DATA_LEN);
    memcpy(raw_data, _MAV_PAYLOAD(msg), len);
#endif
}
