#pragma once
// MESSAGE PARAM_SET PACKING

#define MAVLINK_MSG_ID_PARAM_SET 9

MAVPACKED(
typedef struct __mavlink_param_set_t {
 uint64_t value; /*<  Значение параметра.*/
 uint8_t target; /*<  Идентификатор целевой системы.*/
 uint8_t param; /*<  Идентификатор параметра.*/
 uint8_t type; /*<  Тип параметра.*/
}) mavlink_param_set_t;

#define MAVLINK_MSG_ID_PARAM_SET_LEN 11
#define MAVLINK_MSG_ID_PARAM_SET_MIN_LEN 11
#define MAVLINK_MSG_ID_9_LEN 11
#define MAVLINK_MSG_ID_9_MIN_LEN 11

#define MAVLINK_MSG_ID_PARAM_SET_CRC 173
#define MAVLINK_MSG_ID_9_CRC 173



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_PARAM_SET { \
    9, \
    "PARAM_SET", \
    4, \
    {  { "target", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_set_t, target) }, \
         { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_set_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_param_set_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_set_t, value) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_PARAM_SET { \
    "PARAM_SET", \
    4, \
    {  { "target", NULL, MAVLINK_TYPE_UINT8_T, 0, 8, offsetof(mavlink_param_set_t, target) }, \
         { "param", NULL, MAVLINK_TYPE_UINT8_T, 0, 9, offsetof(mavlink_param_set_t, param) }, \
         { "type", NULL, MAVLINK_TYPE_UINT8_T, 0, 10, offsetof(mavlink_param_set_t, type) }, \
         { "value", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_param_set_t, value) }, \
         } \
}
#endif

/**
 * @brief Pack a param_set message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_set_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target, uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, target);
    _mav_put_uint8_t(buf, 9, param);
    _mav_put_uint8_t(buf, 10, type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_SET_LEN);
#else
    mavlink_param_set_t packet;
    packet.value = value;
    packet.target = target;
    packet.param = param;
    packet.type = type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_SET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_SET;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
}

/**
 * @brief Pack a param_set message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_set_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target,uint8_t param,uint8_t type,uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, target);
    _mav_put_uint8_t(buf, 9, param);
    _mav_put_uint8_t(buf, 10, type);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_SET_LEN);
#else
    mavlink_param_set_t packet;
    packet.value = value;
    packet.target = target;
    packet.param = param;
    packet.type = type;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_SET_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_SET;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
}

/**
 * @brief Encode a param_set struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param param_set C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_set_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_param_set_t* param_set)
{
    return mavlink_msg_param_set_pack(system_id, component_id, msg, param_set->target, param_set->param, param_set->type, param_set->value);
}

/**
 * @brief Encode a param_set struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param_set C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_set_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_param_set_t* param_set)
{
    return mavlink_msg_param_set_pack_chan(system_id, component_id, chan, msg, param_set->target, param_set->param, param_set->type, param_set->value);
}

/**
 * @brief Send a param_set message
 * @param chan MAVLink channel to send the message
 *
 * @param target  Идентификатор целевой системы.
 * @param param  Идентификатор параметра.
 * @param type  Тип параметра.
 * @param value  Значение параметра.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_param_set_send(mavlink_channel_t chan, uint8_t target, uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_SET_LEN];
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, target);
    _mav_put_uint8_t(buf, 9, param);
    _mav_put_uint8_t(buf, 10, type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET, buf, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
#else
    mavlink_param_set_t packet;
    packet.value = value;
    packet.target = target;
    packet.param = param;
    packet.type = type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET, (const char *)&packet, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
#endif
}

/**
 * @brief Send a param_set message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_param_set_send_struct(mavlink_channel_t chan, const mavlink_param_set_t* param_set)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_param_set_send(chan, param_set->target, param_set->param, param_set->type, param_set->value);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET, (const char *)param_set, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
#endif
}

#if MAVLINK_MSG_ID_PARAM_SET_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_param_set_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target, uint8_t param, uint8_t type, uint64_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, value);
    _mav_put_uint8_t(buf, 8, target);
    _mav_put_uint8_t(buf, 9, param);
    _mav_put_uint8_t(buf, 10, type);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET, buf, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
#else
    mavlink_param_set_t *packet = (mavlink_param_set_t *)msgbuf;
    packet->value = value;
    packet->target = target;
    packet->param = param;
    packet->type = type;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_SET, (const char *)packet, MAVLINK_MSG_ID_PARAM_SET_MIN_LEN, MAVLINK_MSG_ID_PARAM_SET_LEN, MAVLINK_MSG_ID_PARAM_SET_CRC);
#endif
}
#endif

#endif

// MESSAGE PARAM_SET UNPACKING


/**
 * @brief Get field target from param_set message
 *
 * @return  Идентификатор целевой системы.
 */
static inline uint8_t mavlink_msg_param_set_get_target(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  8);
}

/**
 * @brief Get field param from param_set message
 *
 * @return  Идентификатор параметра.
 */
static inline uint8_t mavlink_msg_param_set_get_param(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  9);
}

/**
 * @brief Get field type from param_set message
 *
 * @return  Тип параметра.
 */
static inline uint8_t mavlink_msg_param_set_get_type(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  10);
}

/**
 * @brief Get field value from param_set message
 *
 * @return  Значение параметра.
 */
static inline uint64_t mavlink_msg_param_set_get_value(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Decode a param_set message into a struct
 *
 * @param msg The message to decode
 * @param param_set C-struct to decode the message contents into
 */
static inline void mavlink_msg_param_set_decode(const mavlink_message_t* msg, mavlink_param_set_t* param_set)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    param_set->value = mavlink_msg_param_set_get_value(msg);
    param_set->target = mavlink_msg_param_set_get_target(msg);
    param_set->param = mavlink_msg_param_set_get_param(msg);
    param_set->type = mavlink_msg_param_set_get_type(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_PARAM_SET_LEN? msg->len : MAVLINK_MSG_ID_PARAM_SET_LEN;
        memset(param_set, 0, MAVLINK_MSG_ID_PARAM_SET_LEN);
    memcpy(param_set, _MAV_PAYLOAD(msg), len);
#endif
}
